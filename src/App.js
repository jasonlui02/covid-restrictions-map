import "./App.css";
import React, { useEffect, useState, useContext } from "react";
import Map from "./Map";

function App() {
  return (
    <div className="App">
      <div id="map">
        <Map />
      </div>
    </div>
  );
}

export default App;
