import React from "react";
import { MapContainer, GeoJSON, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import data from "./data/countries.json";

const Map = () => {
  console.log(data);
  return (
    <MapContainer style={{ height: "100vh" }} zoom={2} center={[51.505, -0.09]}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {data.features.map((feature, i) => {
        return <GeoJSON key={i} style={{ color: "red" }} data={feature} />;
      })}
    </MapContainer>
  );
};

export default Map;
